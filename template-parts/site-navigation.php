<?php
/**
 * Template part for off canvas menu
 *
 * @package CommonsWP
 * @since CommonsWP 1.0.0
 */

?>


<header class="header">
	<h1 class="hide"><?php bloginfo( 'name' ); ?>
		<small>Une introduction à la notion de Communs</small></h1>
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_portail.png" alt="Le portail des communs"></a>
</header>
<nav id="main-navigation" class="main-navigation content-bar" role="navigation">
	<div class="content-bar-right">
		<?php foundationpress_content_bar_r(); ?>

		<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) == 'contentbar' ) : ?>
			<?php get_template_part( 'template-parts/mobile-content-bar' ); ?>
		<?php endif; ?>
	</div>
</nav>
