# Thème wordpress pour Lescommuns.org

## Fonctionnement

Pour le moment seul le template `front` fonctionne correctement : il ne contient pas de sidebar, mais affiche la bannère et le menu secondaire.

## Menus

Deux menus sont configurés sur le site

* Portail : nav bar pour aller vers les autres sites, dans `Right top bar`
* Main : menu principal, dans `Content Bar` et `Mobile`

## Credits

This wordpress theme is based on the [FoundationPress](https://foundationpress.olefredrik.com/) starter theme.

It uses :
- [Foundation for Sites 6](http://foundation.zurb.com/sites.html) as Sass framework
- Gulp for compiling assets (CSS, js...)

To work on the theme :
- Make a working local install of [Wordpress](https://wordpress.org/)
- In the Theme folder, clone this repository
- "cd" in the cloned repository folder, and "npm install"
- "npm run watch" will launch the watch task that takes care of compiling. From the original gulpfile.js, browser-sync has been commented out, but use what ever sync/reload process you like
- "npm run build" or "npm run production" will build, minified, with no sourcemaps... See [FoundationPress github](https://github.com/olefredrik/FoundationPress) for more details on tasks and folders structure

This site is maintained by an open community :

Contact : question@lescommuns.org
