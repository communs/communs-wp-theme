<?php
/**
 * Add new images sizes
 *
 * @package WordPress
 * @subpackage CommonsWPTheme
 * @since CommonsWPTheme 1
 */

// Add additional image sizes
add_image_size( 'book-cover', 150 );

// Register
add_filter( 'image_size_names_choose', 'communs_wp_sizes' );
function communs_wp_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'book-cover' => __('Book cover'),
    ) );
}
